import numpy as np
import math

inputParams = input().split(' ')
n = int(inputParams[0])

matrix = np.array([np.array([float(x) for x in input().split(' ')])],float)
for i in range(n-1):
    matrix = np.append(matrix,[np.array([int(x) for x in input().split(' ')])],0)

#заполняем несуществующие расстояния на inf
for i in range(n):
    for j in range(n):
        if i!=j and matrix[i][j] == 0 :
            matrix[i][j] = math.inf
#запускаем Алгоритм Флойда — Уоршелла для нахождения кратчайших расстояний между несвязаными вершинами
for k in range(n):
    for i in range(n):
        for j in range(n):
            matrix[i][j] = min(matrix[i][j], matrix[i][k] + matrix[k][j])

#далее запускаем жадный алгоритм "Алгоритм ближайшего соседа"
# в routes будут ве найденные маршруты
routes = np.array([[0]],int)
currentIndex = 0
while len(routes[0])<n :
    temps = routes
    for i in range(len(routes)) :
        d = dict()
        row = np.array([],int)
        for k in [x for x in range(n) if x not in routes[i]] :
            row = np.append(row,matrix[routes[i][currentIndex],k])
            d[k] = matrix[routes[i][currentIndex],k]
        nearlyPointLen = row.min()
        currentPoints = [ key for key,val in d.items() if val==nearlyPointLen ]
        tempRoute = routes[i]
        newrow = np.append(tempRoute,currentPoints[0])
        if i==0 :
            temps = np.array([newrow],int)
        else :
            temps = np.append(temps,[newrow],0)
        
        if len(currentPoints)>1:
            for j in range(len(currentPoints)-1):
                newrow = np.append(tempRoute,currentPoints[j+1])
                temps = np.append(temps,[newrow],0)
    routes = temps
    currentIndex = currentIndex + 1

# получили в массиве routes лучшие маршруты,
#теперь посчитаем длину каждого из них и выберем минимальный
z = np.zeros((len(routes),1), int)
routes = np.append(routes,z,1)
lengths = np.array([],int)
for r in routes :
    length = 0
    for i in range(n) :
        length = length + matrix[r[i],r[i+1]]
    lengths = np.append(lengths,length)
print(int(min(lengths)))

    
